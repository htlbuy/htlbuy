﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTLBuy.Models.db {
    interface IUsersRepo {
        void Open();
        void Close();

        bool Insert(User userToInsert);
        bool Delete(int userIDToDelete);
        bool Authenticate(string usernameOrEMail, string password);
        User GetUser(int userId);
        User GetUser(String user);
        int GetUserId(String user);
        List<User> GetAllUsers();
        bool ChangeUserData(int userIdToChange, User newUserData);
        bool UsernameExists(String username);
        List<Artikel> GetWarenkorbFromUser(String user);
        bool InsertIntoWarenkorb(int userId, int artikelId);
        List<Artikel> GetArtikelFromUser(String user);
    }
}
