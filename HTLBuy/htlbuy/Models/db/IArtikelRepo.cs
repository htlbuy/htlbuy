﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTLBuy.Models.db {
    public interface IArtikelRepo {
        void Open();
        void Close();

        bool Insert(Artikel artikelToInsert, int userId);
        bool Delete(int artikelIDToDelete);
        bool ChangeUserData(int artikelIdToChange, Artikel newArtikelData);
        int GetLastInsertedID();

        List<Artikel> GetAllArtikel();
        Artikel GetArtikel(int artikelId);
        List<Artikel> SearchArtikel(String artikelname);

        int GetGroesseID(string groesse);
        List<string> GetAllGroessen();

        int GetKategorieID(string kategorie);
        List<string> GetAllKategorien();
        IEnumerable<string> GetKategorieFromArtikel(int id);
        void SetKategoriesFromMultipleArtikels(List<Artikel> artikel);
        bool DeleteKategoriesFromArtikel(int artikelIdToDeleteKategories);
    }
}
