﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace HTLBuy.Models.db {
    public class ArtikelRepoDB : IArtikelRepo {

        private string ConnectionString = "Server=localhost;Port=3306;Database=htlbuy;Uid=test;Pwd=test;SSLMode=Preferred";
        private MySqlConnection Connection;

        private IUsersRepo userRepository; //= UserRepoDB.Instance;

        public static ArtikelRepoDB RepoInstance = null;

        private ArtikelRepoDB() { }
        public static ArtikelRepoDB Instance {
            get {
                if (RepoInstance == null)
                    RepoInstance = new ArtikelRepoDB();
                return RepoInstance;
            }
        }
        public void Open() {
            try {
                if (this.Connection == null)
                    this.Connection = new MySqlConnection(this.ConnectionString);
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();
            } catch (MySqlException) {
                throw;
            } catch (Exception) {
                throw;
            }
        }

        public void Close() {
            try {
                if (this.Connection == null)
                    if (this.Connection.State == ConnectionState.Open)
                        this.Connection.Close();
            } catch (MySqlException) {
                throw;
            } catch (Exception) {
                throw;
            }
        }

        public bool Insert(Artikel artikelToInsert, int userId, string base64) {
            try {
                MySqlCommand cmdInsert = this.Connection.CreateCommand();
                cmdInsert.CommandText = "INSERT INTO artikel VALUES(null, @name, @beschreibung, @preis, @groesse, " + userId + ", "+ base64 +");";
                cmdInsert.Parameters.AddWithValue("name", artikelToInsert.Name);
                cmdInsert.Parameters.AddWithValue("beschreibung", artikelToInsert.Beschreibung);
                cmdInsert.Parameters.AddWithValue("preis", artikelToInsert.Preis);
                cmdInsert.Parameters.AddWithValue("groesse", GetGroesseID(artikelToInsert.Groesse));
                bool insertedArtikel = cmdInsert.ExecuteNonQuery() == 1;
                int artikelID = GetLastInsertedID();
                bool insertedKategorie = true;

                foreach (string s in artikelToInsert.Kategorie) {
                    MySqlCommand cmdInsertKategorien = this.Connection.CreateCommand();
                    cmdInsertKategorien.CommandText = "INSERT INTO kategorie_artikel VALUES(@artikel, @kategorie);";
                    cmdInsertKategorien.Parameters.AddWithValue("artikel", artikelID);
                    cmdInsertKategorien.Parameters.AddWithValue("kategorie", GetKategorieID(s));
                    if (cmdInsertKategorien.ExecuteNonQuery() != 1)
                        insertedKategorie = false;
                }

                return insertedArtikel && insertedKategorie;
            } catch (MySqlException e) {
                Console.Write(e.Message);
                throw;
            } catch (Exception) {
                throw;
            }
        }

        public bool Delete(int artikelIDToDelete) {
            try {
                MySqlCommand cmdDeleteUser = this.Connection.CreateCommand();
                cmdDeleteUser.CommandText = "DELETE FROM artikel WHERE id=@id";
                cmdDeleteUser.Parameters.AddWithValue("id", artikelIDToDelete);
                cmdDeleteUser.ExecuteReader();
                return true;
            } catch (MySqlException) {
                throw;
            }
        }

        public bool ChangeUserData(int artikelIdToChange, Artikel newArtikelData) {
            try {
                MySqlCommand cmdInsert = this.Connection.CreateCommand();
                cmdInsert.CommandText = "UPDATE artikel SET name=@name, beschreibung=@beschreibung, preis=@preis, groesse=@groesse) WHERE id=@id;";
                cmdInsert.Parameters.AddWithValue("name", newArtikelData.Name);
                cmdInsert.Parameters.AddWithValue("beschreibung", newArtikelData.Beschreibung);
                cmdInsert.Parameters.AddWithValue("preis", newArtikelData.Preis);
                cmdInsert.Parameters.AddWithValue("groesse", GetGroesseID(newArtikelData.Groesse));
                cmdInsert.Parameters.AddWithValue("id", artikelIdToChange);
                bool updatedArtikel = cmdInsert.ExecuteNonQuery() == 1;
                bool insertedKategorie = true;

                bool deletedKategories = DeleteKategoriesFromArtikel(artikelIdToChange);

                foreach (string s in newArtikelData.Kategorie) {
                    MySqlCommand cmdInsertKategorien = this.Connection.CreateCommand();
                    cmdInsertKategorien.CommandText = "INSERT INTO kategorie_artikel VALUES(@artikel, @kategorie);";
                    cmdInsertKategorien.Parameters.AddWithValue("artikel", artikelIdToChange);
                    cmdInsertKategorien.Parameters.AddWithValue("kategorie", GetKategorieID(s));
                    if (cmdInsertKategorien.ExecuteNonQuery() != 1)
                        insertedKategorie = false;
                }

                return updatedArtikel && insertedKategorie && deletedKategories;
            } catch (MySqlException e) {
                Console.Write(e.Message);
                throw;
            } catch (Exception) {
                throw;
            }
        }

        public int GetLastInsertedID() {
            try {
                MySqlCommand cmdSelect = this.Connection.CreateCommand();
                cmdSelect.CommandText = "SELECT LAST_INSERT_ID();";

                //"kurze" schreibweise für try finally
                using (MySqlDataReader reader = cmdSelect.ExecuteReader()) {
                    reader.Read();
                    return Convert.ToInt32(reader["LAST_INSERT_ID()"]);
                }
            } catch (MySqlException) {
                throw;
            }
        }

        public List<Artikel> GetAllArtikel() {
            List<Artikel> allArtikel = new List<Artikel>();
            try {
                MySqlCommand cmdSelectArtikel = this.Connection.CreateCommand();
                cmdSelectArtikel.CommandText = "SELECT * FROM artikel;";

                using (MySqlDataReader reader = cmdSelectArtikel.ExecuteReader()) {
                    while (reader.Read()) {
                        allArtikel.Add(
                            new Artikel(Convert.ToInt32(reader["id"]),
                            Convert.ToString(reader["name"]),
                            Convert.ToString(reader["beschreibung"]),
                            Convert.ToDecimal(reader["preis"]),
                            Convert.ToString(reader["groesse"]),
                            null, null));
                    }
                }
                SetKategoriesFromMultipleArtikels(allArtikel);
            } catch (MySqlException) {
                throw;
            }
            return allArtikel.Count >= 1 ? allArtikel : null;
        }

        public Artikel GetArtikel(int artikelId) {
            Artikel artikel = null;
            try {
                MySqlCommand cmdSelectArtikel = this.Connection.CreateCommand();
                cmdSelectArtikel.CommandText = "SELECT * FROM artikel WHERE id=@id;";
                cmdSelectArtikel.Parameters.AddWithValue("id", artikelId);

                using (MySqlDataReader reader = cmdSelectArtikel.ExecuteReader()) {
                    reader.Read();
                    artikel = new Artikel(
                            Convert.ToInt32(reader["id"]),
                            Convert.ToString(reader["name"]),
                            Convert.ToString(reader["beschreibung"]),
                            Convert.ToDecimal(reader["preis"]),
                            Convert.ToString(reader["groesse"]),
                            null, null);
                }
                artikel.Kategorie = GetKategorieFromArtikel(artikel.ID);
            } catch (MySqlException) {
                throw;
            }
            return artikel == null ? null : artikel;
        }

        public List<Artikel> SearchArtikel(string artikelname) {
            List<Artikel> allArtikel = new List<Artikel>();
            try {
                MySqlCommand cmdSelectArtikel = this.Connection.CreateCommand();
                cmdSelectArtikel.CommandText = "SELECT * FROM artikel WHERE name LIKE @name;";
                cmdSelectArtikel.Parameters.AddWithValue("name", "%" + artikelname + "%");

                using (MySqlDataReader reader = cmdSelectArtikel.ExecuteReader()) {
                    while (reader.Read()) {
                        allArtikel.Add(
                            new Artikel(Convert.ToInt32(reader["id"]),
                            Convert.ToString(reader["name"]),
                            Convert.ToString(reader["beschreibung"]),
                            Convert.ToDecimal(reader["preis"]),
                            Convert.ToString(reader["groesse"]),
                            null, null));
                    }
                }
                SetKategoriesFromMultipleArtikels(allArtikel);
            } catch (MySqlException) {
                throw;
            }
            return allArtikel.Count >= 1 ? allArtikel : null;
        }

        public int GetGroesseID(string groesse) {
            try {
                MySqlCommand cmdSelect = this.Connection.CreateCommand();
                cmdSelect.CommandText = "SELECT id FROM groesse WHERE groesse=@groesse;";
                cmdSelect.Parameters.AddWithValue("groesse", groesse);

                //"kurze" schreibweise für try finally
                using (MySqlDataReader reader = cmdSelect.ExecuteReader()) {
                    reader.Read();
                    return Convert.ToInt32(reader["id"]);
                }
            } catch (MySqlException) {
                throw;
            }
        }

        public List<string> GetAllGroessen() {
            List<string> allGroessen = new List<string>();
            try {
                MySqlCommand cmdSelectAllGroessen = this.Connection.CreateCommand();
                cmdSelectAllGroessen.CommandText = "SELECT * FROM groesse";

                //"kurze" schreibweise für try finally
                using (MySqlDataReader reader = cmdSelectAllGroessen.ExecuteReader()) {
                    while (reader.Read()) {
                        allGroessen.Add(Convert.ToString(reader["groesse"]));
                    }
                }
            } catch (MySqlException) {
                throw;
            }
            return allGroessen.Count >= 1 ? allGroessen : null;
        }

        public int GetKategorieID(string kategorie) {
            try {
                MySqlCommand cmdSelect = this.Connection.CreateCommand();
                cmdSelect.CommandText = "SELECT id FROM kategorie WHERE name=@name;";
                cmdSelect.Parameters.AddWithValue("name", kategorie);

                //"kurze" schreibweise für try finally
                using (MySqlDataReader reader = cmdSelect.ExecuteReader()) {
                    reader.Read();
                    return Convert.ToInt32(reader["id"]);
                }
            } catch (MySqlException) {
                throw;
            }
        }

        public List<string> GetAllKategorien() {
            List<string> allKategorien = new List<string>();
            try {
                MySqlCommand cmdSelectAllKategorien = this.Connection.CreateCommand();
                cmdSelectAllKategorien.CommandText = "SELECT * FROM kategorie;";

                //"kurze" schreibweise für try finally
                using (MySqlDataReader reader = cmdSelectAllKategorien.ExecuteReader()) {
                    while (reader.Read()) {
                        allKategorien.Add(Convert.ToString(reader["name"]));
                    }
                }
            } catch (MySqlException) {
                throw;
            }
            return allKategorien.Count >= 1 ? allKategorien : null;
        }

        public IEnumerable<string> GetKategorieFromArtikel(int id) {
            List<string> allKategorien = new List<string>();
            try {
                MySqlCommand cmdSelectAllKategorien = this.Connection.CreateCommand();
                cmdSelectAllKategorien.CommandText = "SELECT * FROM kategorie_artikel WHERE artikel=@id;";
                cmdSelectAllKategorien.Parameters.AddWithValue("id", id);

                using (MySqlDataReader reader = cmdSelectAllKategorien.ExecuteReader()) {
                    while (reader.Read()) {
                        allKategorien.Add(Convert.ToString(reader["kategorie"]));
                    }
                }
            } catch (MySqlException) {
                throw;
            }
            return allKategorien.Count >= 1 ? allKategorien : null;
        }

        public void SetKategoriesFromMultipleArtikels(List<Artikel> artikel) {
            foreach (Artikel a in artikel) {
                a.Kategorie = GetKategorieFromArtikel(a.ID);
            }
        }

        public bool DeleteKategoriesFromArtikel(int artikelIdToDeleteKategories) {
            bool deleteSuccess = true;
            foreach (string s in GetKategorieFromArtikel(artikelIdToDeleteKategories)) {
                MySqlCommand cmdDeleteKategorieFromArtikel = this.Connection.CreateCommand();
                cmdDeleteKategorieFromArtikel.CommandText = "DELETE FROM kategorie_artikel WHERE artikel=@artikel AND kategorie=@kategorie);";
                cmdDeleteKategorieFromArtikel.Parameters.AddWithValue("artikel", artikelIdToDeleteKategories);
                cmdDeleteKategorieFromArtikel.Parameters.AddWithValue("kategorie", GetKategorieID(s));
                if (cmdDeleteKategorieFromArtikel.ExecuteNonQuery() != 1)
                    deleteSuccess = false;
            }
            return deleteSuccess;
        }

    }
}