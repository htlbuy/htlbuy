﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTLBuy.Models {
    public class User {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Passwort { get; set; }
        public string EMail { get; set; }
        public DateTime GebTag { get; set; }

        public User() : this("", "", "", new DateTime(2001, 1, 1)) { }
        public User(string username, string passwort, string email, DateTime geb) {
            this.Id = -1;
            this.Username = username;
            this.Passwort = passwort;
            this.EMail = email;
            this.GebTag = geb;
        }
    }
}