﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTLBuy.Models.ViewModels {
    public class CreateArticleViewModel {
        public Artikel Artikel { get; set; }
        public IEnumerable<string> Kategorien { get; set; }
        public IEnumerable<string> Groesse { get; set; }
    }
}