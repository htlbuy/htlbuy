﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTLbuy.Models
{
    public class Category
    {
        // properties
        public int ID { get; set; }
        public string Name { get; set; }

        // ctors
        public Category() : this(0, "") { }
        public Category(int id, string name)
        {
            this.ID = id;
            this.Name = name;          
        }

        // ToString()
        public override string ToString()
        {
            return this.ID + "   " + this.Name;
        }

    }
}
