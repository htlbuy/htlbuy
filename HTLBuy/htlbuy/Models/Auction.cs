﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HTLBuy.Models;

namespace HTLbuy.Models
{
    public class Auction
    {
        // fields
        private decimal _price;

        // properties
        public int ID { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Category> Categories { get; set; }
        public User AuctionOwner { get; set; }
        public User HighestBidder { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Price
        {
            get { return this._price; }
            set
            {
                if (value >= 0.0m)
                {
                    this._price = value;
                }
            }
        }

        // ctors
        public Auction() : this(0, "", "", "", 0.0m, null, null, null, DateTime.MinValue, DateTime.MinValue) { }
        public Auction(int id, string image, string name, string description, decimal price, 
            List<Category> categories, User auctionOwner, User highestBidder, DateTime startDate, DateTime endDate)
        {
            this.ID = id;
            this.Image = image;
            this.Name = name;
            this.Description = description;
            this.Price = price;            
            if (categories == null)
            {
                this.Categories = new List<Category>();
            }
            else
            {
                this.Categories = categories;
            }
            this.AuctionOwner = auctionOwner;
            this.HighestBidder = highestBidder;
            this.StartDate = startDate;
            this.EndDate = endDate;
        }

        // ToString()
        public override string ToString()
        {
            return this.ID + " " + this.Name + Environment.NewLine +
                this.Description + Environment.NewLine +
                this.Price + "€" + Environment.NewLine +
                "von " + this.StartDate + " bis " + this.EndDate + Environment.NewLine +
                "Bild: " + this.Image;
        }

    }
}
