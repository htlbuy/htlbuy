-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: HTLBuy
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `artikel`
--

CREATE DATABASE IF NOT EXISTS htlbuy;
USE htlbuy;

DROP TABLE IF EXISTS `artikel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `artikel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `beschreibung` varchar(1000) NOT NULL,
  `preis` double NOT NULL,
  `groesse` int(11) DEFAULT NULL,
  `verkaufer` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `groesse_FK` (`groesse`),
  CONSTRAINT `groesse_FK` FOREIGN KEY (`groesse`) REFERENCES `groesse` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artikel`
--

LOCK TABLES `artikel` WRITE;
/*!40000 ALTER TABLE `artikel` DISABLE KEYS */;
INSERT INTO `artikel` VALUES (1,'test','bdansbhd',2,2,NULL),(2,'test2','akjsdjka',3,5,NULL),(3,'test2','akjsdjka',3,5,NULL),(4,'test2','akjsdjka',3,5,NULL),(5,'test2','akjsdjka',3,5,NULL),(6,'test2','akjsdjka',3,5,NULL),(7,'test2','akjsdjka',3,5,NULL),(8,'Auto','Mercedes',2000,4,NULL);
/*!40000 ALTER TABLE `artikel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groesse`
--

DROP TABLE IF EXISTS `groesse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `groesse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groesse` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groesse`
--

LOCK TABLES `groesse` WRITE;
/*!40000 ALTER TABLE `groesse` DISABLE KEYS */;
INSERT INTO `groesse` VALUES (1,'notSpezified'),(2,'small'),(3,'medium'),(4,'large'),(5,'xlarge');
/*!40000 ALTER TABLE `groesse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorie`
--

DROP TABLE IF EXISTS `kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `kategorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorie`
--

LOCK TABLES `kategorie` WRITE;
/*!40000 ALTER TABLE `kategorie` DISABLE KEYS */;
INSERT INTO `kategorie` VALUES (1,'Technik'),(2,'Computer'),(3,'Kleidung'),(4,'Bücher'),(5,'Schuhe'),(6,'Spielzeug');
/*!40000 ALTER TABLE `kategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorie_artikel`
--

DROP TABLE IF EXISTS `kategorie_artikel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `kategorie_artikel` (
  `artikel` int(11) NOT NULL,
  `kategorie` int(11) NOT NULL,
  KEY `artikel_FK` (`artikel`),
  KEY `kategorie_FK` (`kategorie`),
  CONSTRAINT `artikel_FK` FOREIGN KEY (`artikel`) REFERENCES `artikel` (`id`),
  CONSTRAINT `kategorie_FK` FOREIGN KEY (`kategorie`) REFERENCES `kategorie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorie_artikel`
--

LOCK TABLES `kategorie_artikel` WRITE;
/*!40000 ALTER TABLE `kategorie_artikel` DISABLE KEYS */;
INSERT INTO `kategorie_artikel` VALUES (7,3),(8,1);
/*!40000 ALTER TABLE `kategorie_artikel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `passwrd` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `birthdate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (5,'admin','6c7ca345f63f835cb353ff15bd6c5e052ec08e7a','admin@admin.at','0404-04-04'),(7,'testUser1','b444ac06613fc8d63795be9ad0beaf55011936ac','test@test.com','2018-12-11'),(8,'Ramagos','5a00bfd4cba30f607ee98641cb11ec2a1572edac','samuel.flatscher@netgraf.at','2000-08-16');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warenkorb`
--

DROP TABLE IF EXISTS `warenkorb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `warenkorb` (
  `user` int(11) NOT NULL,
  `artikel` int(11) NOT NULL,
  KEY `warenkorb_user_FK` (`user`),
  KEY `warenkorb_artikel_FK` (`artikel`),
  CONSTRAINT `warenkorb_artikel_FK` FOREIGN KEY (`artikel`) REFERENCES `artikel` (`id`),
  CONSTRAINT `warenkorb_user_FK` FOREIGN KEY (`user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warenkorb`
--

LOCK TABLES `warenkorb` WRITE;
/*!40000 ALTER TABLE `warenkorb` DISABLE KEYS */;
INSERT INTO `warenkorb` VALUES (8,1),(8,1);
/*!40000 ALTER TABLE `warenkorb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-08  9:50:16
