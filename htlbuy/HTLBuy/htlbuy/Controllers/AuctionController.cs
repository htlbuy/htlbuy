﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using HTLbuy.Models;


using HTLBuy.Models.db;



namespace HTLBuy.Controllers
{
    public class AuctionController : Controller
    {

        IAuctionsRepository auctionRepository = AuctionRepositoryDB.Instance;
        // GET: Auction
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult NewAuction()
        {
            Auction a = new Auction();
            return View(a);
        }

        [HttpPost]
        public ActionResult NewAuction (Auction a)
        {
            try
            {
                auctionRepository.Open();

                if (ModelState.IsValid)
                {
                    Auction au = new Auction(a.ID, a.Image, a.Name, a.Description, a.Price, a.Categories, a.AuctionOwner, a.HighestBidder, a.StartDate, a.EndDate);

                }
                return View(a);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                auctionRepository.Close();
            }
        }

    }
}