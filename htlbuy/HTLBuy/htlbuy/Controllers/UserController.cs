﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTLBuy.Models;
using HTLBuy.Models.db;
using MySql.Data.MySqlClient;

namespace HTLBuy.Controllers {
    public class UserController : Controller {
        IUsersRepo usersRepository = UserRepoDB.Instance;

        // GET: User
        public ActionResult Index() {
            return View();
        }

        public ActionResult RegisteredUser() {
            try {
                usersRepository.Open();
                return View(usersRepository.GetAllUsers());
            } catch (Exception) {
                throw;
            } finally {
                usersRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult EditUser(String id) {
            try {
                usersRepository.Open();
                User u = usersRepository.GetUser(id);
                return View(u);
            } catch (Exception) {
                throw;
            } finally {
                usersRepository.Close();
            }
        }
        [HttpPost]
        public ActionResult EditUser(User u) {
            try {
                usersRepository.Open();

                if ((u.Username == null) || (u.Username.Trim().Length < 3)) {
                    ModelState.AddModelError("User.Username", "Username muss mindestens 3 Zeichen lang sein");
                }
                if (u.EMail == null) {
                    ModelState.AddModelError("User.Email", "Bitte geben sie eine E-Mail ein");
                }
                if (u.GebTag > DateTime.Now) {
                    ModelState.AddModelError("User.GebTag", "Bitte geben sie ein Datum in der Vergangenheit an");
                }
                if (usersRepository.GetUser(u.Id).Username != u.Username)
                    if (usersRepository.UsernameExists(u.Username)) {
                        ModelState.AddModelError("User.Username", "Username bereits vergeben");
                    }
                if (ModelState.IsValid) {
                    try {
                        usersRepository = UserRepoDB.Instance;
                        usersRepository.Open();

                        if (usersRepository.ChangeUserData(u.Id, u)) {
                            return View("Message", new Message("Userverwaltung", "", "Daten wurden erfolgreich geändert", ""));
                        } else {
                            return View("Message", new Message("Userverwaltung", "", "Daten konnten nicht geändert werden", "Bitte versuchen sie es später noch einmal"));
                        }

                    } catch (MySqlException) {
                        return View("Message", new Message("Datenbankfehler", "", "Problem mit der Datenbankverbindung!", "Versuchen sie es später erneut!"));
                    } catch (Exception) {
                        return View("Message", new Message("unbekannter Fehler", "", "unbekannter Fehler", "Versuchen sie es später erneut!"));
                    } finally {
                        usersRepository.Close();
                    }
                }
                return View(u);
            } catch (Exception) {
                throw;
            } finally {
                usersRepository.Close();
            }
        }

        [HttpGet]
        public ActionResult Login() {
            User u = new User();
            return View(u);
        }
        [HttpPost]
        public ActionResult Login(User u) {
            try {
                usersRepository.Open();

                if ((u.Username == null) || (u.Username.Trim().Length < 3)) {
                    ModelState.AddModelError("User.Username", "Artikelname muss mindestens 3 Zeichen lang sein");
                }
                if ((u.Passwort == null) || (u.Passwort.Trim().Length < 5)) {
                    ModelState.AddModelError("User.Passwort", "Passwort muss mindestens 5 Zeichen lang sein");
                }
                if (!usersRepository.Authenticate(u.Username, u.Passwort)) {
                    ModelState.AddModelError("User", "Benutzername/EMail oder Passwort sind nicht korrekt");
                }
                if (ModelState.IsValid) {
                    Session["Username"] = u.Username;
                    Session["isLoggedIn"] = true;
                    return View("Message", new Message("Login", "", "Sie wurden erfolgreich eingeloggt. Willkommen " + u.Username, ""));
                }
                return View(u);
            } catch (Exception) {
                throw;
            } finally {
                usersRepository.Close();
            }
        }
        [HttpGet]
        public ActionResult Register() {
            User u = new User();
            return View(u);
        }
        [HttpPost]
        public ActionResult Register(User u) {
            try {
                usersRepository.Open();
                if ((u.Username == null) || (u.Username.Trim().Length < 3)) {
                    ModelState.AddModelError("User.Username", "Username muss mindestens 3 Zeichen lang sein");
                }
                if ((u.Passwort == null) || (u.Passwort.Trim().Length < 5)) {
                    ModelState.AddModelError("User.Passwort", "Passwort muss mindestens 5 Zeichen lang sein");
                }
                if (u.EMail == null) {
                    ModelState.AddModelError("User.Email", "Bitte geben sie eine E-Mail ein");
                }
                if (u.GebTag > DateTime.Now) {
                    ModelState.AddModelError("User.GebTag", "Bitte geben sie ein Datum in der Vergangenheit an");
                }
                if (usersRepository.UsernameExists(u.Username)) {
                    ModelState.AddModelError("User.Username", "Username bereits vergeben");
                }
                if (ModelState.IsValid) {
                    try {
                        usersRepository = UserRepoDB.Instance;
                        usersRepository.Open();

                        if (usersRepository.Insert(u)) {
                            Session["Username"] = u.Username;
                            Session["isLoggedIn"] = true;
                            return View("Message", new Message("Registrierung", "", "Sie wurden erfolgreich registriert", ""));
                        } else {
                            return View("Message", new Message("Registrierung", "", "Sie konnten nicht registriert werden", "Bitte versuchen sie es später noch einmal"));
                        }

                    } catch (MySqlException) {
                        return View("Message", new Message("Datenbankfehler", "", "Problem mit der Datenbankverbindung!", "Versuchen sie es später erneut!"));
                    } catch (Exception) {
                        return View("Message", new Message("unbekannter Fehler", "", "unbekannter Fehler", "Versuchen sie es später erneut!"));
                    } finally {
                        usersRepository.Close();
                    }
                }
                return View(u);
            } catch (Exception) {
                throw;
            } finally {
                usersRepository.Close();
            }
        }
        public ActionResult Logout() {
            Session["Username"] = null;
            Session["isLoggedIn"] = false;
            return View("Message", new Message("Logout", "", "Sie wurden erfolgreich ausgeloggt!", ""));
        }
        public ActionResult DeleteUser(int id) {
            try {
                usersRepository.Open();

                usersRepository.Delete(id);
                return View("Message", new Message("Userverwaltung", "", "User gelöscht", ""));
            } catch (Exception) {
                throw;
            } finally {
                usersRepository.Close();
            }
        }
        public ActionResult Warenkorb() {
            try {
                usersRepository.Open();

                List<Artikel> artikel = usersRepository.GetWarenkorbFromUser(Convert.ToString(Session["Username"]));
                if ((artikel != null) && (artikel.Count > 0))
                    return View("Liste", artikel);
                else
                    return View("Message", new Message("Warenkorb", "", "In Ihrem Warenkorb befinden sich keine Artikel", ""));
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            } finally {
                usersRepository.Close();
            }

        }
        public ActionResult Myartikel() {
            try {
                usersRepository.Open();

                List<Artikel> artikel = usersRepository.GetArtikelFromUser(Convert.ToString(Session["Username"]));
                if ((artikel != null) && (artikel.Count > 0))
                    return View("Liste", artikel);
                else
                    return View("Message", new Message("Myartikel", "", "Sie verkaufen gerade keine Artikel", ""));
            } catch (Exception) {
                throw;
            } finally {
                usersRepository.Close();
            }
        }
    }
}