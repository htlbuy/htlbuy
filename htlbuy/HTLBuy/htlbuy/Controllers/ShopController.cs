﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTLBuy.Models;
using HTLBuy.Models.ViewModels;
using HTLBuy.Models.db;
using MySql.Data.MySqlClient;

namespace HTLBuy.Controllers {
    public class ShopController : Controller {
        IArtikelRepo artikelRepository = ArtikelRepoDB.Instance;
        IUsersRepo usersRepository = UserRepoDB.Instance;
        // GET: Shop
        public ActionResult Add() {
            return View();
        }
        public ActionResult Liste() {
            return View(GetArtikelList());
        }

        [HttpGet]
        public ActionResult Search() {
            SearchViewModel sVM = new SearchViewModel() {
                Name = ""
            };
            return View(sVM);
        }
        [HttpPost]
        public ActionResult Search(SearchViewModel sVM) {
            if ((sVM.Name == null) || (sVM.Name == "")) {
                ModelState.AddModelError("Input", "Bitte geben sie einen Suchbegriff ein.");
            }
            if (ModelState.IsValid) {
                List<Artikel> artikel = SearchArtikelList(sVM.Name);
                if ((artikel != null) && (artikel.Count > 0))
                    return View("Liste", artikel);
                else
                    ModelState.AddModelError("Liste", "Leider keine Artikel vorhanden");
            }
            return View(sVM);
        }
        public class EditUserProfileViewModel {
            public string Name { get; set; }
        }

        [HttpGet]
        public ActionResult CreateArticle() {
            CreateArticleViewModel aVM = new CreateArticleViewModel() {
                Artikel = new Artikel(),
                Kategorien = GetCategoriesFromDB(),
                Groesse = GetGroessenFromDB()
            };
            return View(aVM);
        }

        [HttpPost]
        public ActionResult CreateArticle(CreateArticleViewModel art) {
            if ((art.Artikel.Name == null) || (art.Artikel.Name.Trim().Length < 3)) {
                ModelState.AddModelError("Artikel.Name", "Artikelname muss mindestens 3 Zeichen lang sein");
            }
            if (art.Artikel.Preis <= 0) {
                ModelState.AddModelError("Artikel.Preis", "Preis muss über 0€ betragen");
            }
            if (art.Artikel.Groesse == "notSpecified") {
                ModelState.AddModelError("Artikel.Groesse", "Wählen sie bitte eine Groesse");
            }
            if (art.Artikel.Kategorie.Count() < 1) {
                ModelState.AddModelError("Artikel.Kategorie", "Wählen sie bitte eine oder mehrere Kategorien");
            }
            if (ModelState.IsValid) {
                try {
                    artikelRepository.Open();
                    usersRepository.Open();

                    Artikel a = new Artikel(art.Artikel.ID, art.Artikel.Name, art.Artikel.Beschreibung, art.Artikel.Preis, art.Artikel.Groesse, art.Artikel.Kategorie);

                    if (artikelRepository.Insert(a, usersRepository.GetUserId(Convert.ToString(Session["Username"])))) {
                        return View("Message", new Message("Artikelerzeugung", "", "Der Artikel wurde erfolgreich abgespeichert", ""));
                    } else {
                        return View("Message", new Message("Artikelerzeugung", "", "Der Artikel konnte nicht abgespeichert werden", "Bitte versuchen sie es später noch einmal"));
                    }

                } catch (MySqlException) {
                    return View("Message", new Message("Datenbankfehler", "", "Problem mit der Datenbankverbindung!", "Versuchen sie es später erneut!"));
                } catch (Exception) {
                    return View("Message", new Message("unbekannter Fehler", "", "unbekannter Fehler", "Versuchen sie es später erneut!"));
                } finally {
                    artikelRepository.Close();
                    usersRepository.Close();
                }
            }
            art.Groesse = GetGroessenFromDB();
            art.Kategorien = GetCategoriesFromDB();
            return View(art);
        }
        private List<string> GetCategoriesFromDB() {
            try {
                artikelRepository.Open();
                return artikelRepository.GetAllKategorien();
            } catch (Exception) {
                throw;
            } finally {
                artikelRepository.Close();
            }
        }

        private List<string> GetGroessenFromDB() {
            try {
                artikelRepository.Open();
                return artikelRepository.GetAllGroessen();
            } catch (Exception) {
                throw;
            } finally {
                artikelRepository.Close();
            }
        }

        private List<Artikel> GetArtikelList() {
            try {
                artikelRepository.Open();
                return artikelRepository.GetAllArtikel();
            } catch (Exception) {
                throw;
            } finally {
                artikelRepository.Close();
            }
        }
        private List<Artikel> SearchArtikelList(String name) {
            try {
                artikelRepository.Open();
                return artikelRepository.SearchArtikel(name);
            } catch (Exception) {
                throw;
            } finally {
                artikelRepository.Close();
            }
        }

        public ActionResult ShowArtikel(int Id) {
            try {
                artikelRepository.Open();
                Artikel artikel = artikelRepository.GetArtikel(Id);
                if (artikel != null)
                    return View("ShowArtikel", artikel);
                else
                    return View("Message", new Message("Artikel", "", "Leider keine Artikeldaten vorhanden", ""));
            } catch (Exception) {
                throw;
            } finally {
                artikelRepository.Close();
            }
        }
        public ActionResult ArtikelInWarenkorb(int id) {
            try {
                usersRepository.Open();
                int userId = usersRepository.GetUserId(Convert.ToString(Session["Username"]));
                usersRepository.InsertIntoWarenkorb(userId, id);
                return View("Message", new Message("Warenkorb", "", "Artikel erfolgreich in den Warenkorb gelegt", ""));
            } catch (Exception) {
                throw;
            } finally {
                usersRepository.Close();
            }
        }
    }
}