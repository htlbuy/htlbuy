﻿using System;
using System.IO;
using System.Drawing;



namespace Pic_Base64
{
    public class ImageBase64
    {
       

        public ImageBase64()
        {
        }

       

        public static string ImageToBase64(Image image)
        {
            using (MemoryStream m = new MemoryStream())
            {
                image.Save(m, image.RawFormat);
                byte[] imageBytes = m.ToArray();


                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }
    }
}
