﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTLBuy.Models {
    public class Artikel {
        private decimal preis;

        public int ID { get; set; }
        public string Name { get; set; }
        public string Beschreibung { get; set; }
        public decimal Preis {
            get { return this.preis; }
            set {
                if (value >= 0.0m)
                    this.preis = value;
            }
        }
        public string Groesse { get; set; }
        public IEnumerable<string> Kategorie { get; set; }


        public Artikel() : this(0, "", "", 0.0m, "notSpecified", null) { }
        public Artikel(int id, string name, string beschreibung, decimal preis, string groesse, IEnumerable<string> kategorien) {
            this.ID = id;
            this.Name = name;
            this.Beschreibung = beschreibung;
            this.Preis = preis;
            this.Groesse = groesse;
            if (kategorien == null) {
                this.Kategorie = new List<string>();
            } else {
                this.Kategorie = kategorien;
            }
        }

        public override string ToString() {
            return this.ID + " " + this.Name + " " + this.Preis + "€" + Environment.NewLine + this.Beschreibung + Environment.NewLine;
        }
    }
}