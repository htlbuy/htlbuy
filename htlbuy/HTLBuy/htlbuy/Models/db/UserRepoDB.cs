﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace HTLBuy.Models.db {
    public class UserRepoDB : IUsersRepo {

        private string ConnectionString = "Server=localhost;Port=3306;Database=htlbuy;Uid=root;Pwd=sammy2000;SSLMode=Preferred";
        private MySqlConnection Connection;
        public static int bla;

        private static UserRepoDB RepoInstance;

        private IArtikelRepo artikelRepository;

        private UserRepoDB() {
            bla = 10;
        }
        public static UserRepoDB Instance {
            get {
                if (RepoInstance == null) {
                    bla = 9;
                    RepoInstance = new UserRepoDB();
                    bla = 11;
                }
                return RepoInstance;
            }
        }

        public void Open() {
            try {
                if (this.Connection == null)
                    this.Connection = new MySqlConnection(this.ConnectionString);
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();
            } catch (MySqlException) {
                throw;
            } catch (Exception) {
                throw;
            }
        }
        public void Close() {
            try {
                if (this.Connection == null)
                    if (this.Connection.State == ConnectionState.Open)
                        this.Connection.Close();
            } catch (MySqlException) {
                throw;
            } catch (Exception) {
                throw;
            }
        }

        public bool Authenticate(string usernameOrEMail, string password) {
            int counter = 0;
            try {
                MySqlCommand cmdAuthenticate = this.Connection.CreateCommand();
                cmdAuthenticate.CommandText = "SELECT * FROM users WHERE username=@userOrEmail AND passwrd=sha1(@pw) " +
                    "OR email=@userOrEmail AND passwrd=sha1(@pw)";
                cmdAuthenticate.Parameters.AddWithValue("userOrEmail", usernameOrEMail);
                cmdAuthenticate.Parameters.AddWithValue("pw", password);

                using (MySqlDataReader reader = cmdAuthenticate.ExecuteReader()) {
                    while (reader.Read()) {
                        counter++;
                    }
                }
            } catch (MySqlException) {
                throw;
            }
            return counter > 0;
        }
        public bool UsernameExists(String username) {
            int counter = 0;
            try {
                MySqlCommand cmdUsernameExists = this.Connection.CreateCommand();
                cmdUsernameExists.CommandText = "SELECT * FROM users WHERE username=@user";
                cmdUsernameExists.Parameters.AddWithValue("user", username);

                using (MySqlDataReader reader = cmdUsernameExists.ExecuteReader()) {
                    while (reader.Read()) {
                        counter++;
                    }
                }
            } catch (MySqlException) {
                throw;
            }
            return counter > 0;
        }

        public bool ChangeUserData(int userIdToChange, User newUserData) {
            try {
                MySqlCommand cmdUpdate = this.Connection.CreateCommand();
                cmdUpdate.CommandText = "UPDATE users SET username=@username, passwrd=sha1(@passwort), email=@email, birthdate=@gebtag WHERE id=@id;";
                cmdUpdate.Parameters.AddWithValue("username", newUserData.Username);
                cmdUpdate.Parameters.AddWithValue("passwort", newUserData.Passwort);
                cmdUpdate.Parameters.AddWithValue("email", newUserData.EMail);
                cmdUpdate.Parameters.AddWithValue("gebtag", newUserData.GebTag);
                cmdUpdate.Parameters.AddWithValue("id", userIdToChange);

                return cmdUpdate.ExecuteNonQuery() == 1;
            } catch (MySqlException) {
                throw;
            } catch (Exception) {
                throw;
            }
        }

        public bool Delete(int userIDToDelete) {
            try {
                MySqlCommand cmdDeleteUser = this.Connection.CreateCommand();
                cmdDeleteUser.CommandText = "DELETE FROM users WHERE id=@id";
                cmdDeleteUser.Parameters.AddWithValue("id", userIDToDelete);
                cmdDeleteUser.ExecuteReader();
                return true;
            } catch (MySqlException) {
                throw;
            }
        }

        public List<User> GetAllUsers() {
            List<User> allUsers = new List<User>();
            try {
                MySqlCommand cmdSelectAllUsers = this.Connection.CreateCommand();
                cmdSelectAllUsers.CommandText = "SELECT * FROM users";

                //"kurze" schreibweise für try finally
                using (MySqlDataReader reader = cmdSelectAllUsers.ExecuteReader()) {
                    while (reader.Read()) {
                        allUsers.Add(
                            new User {
                                Id = Convert.ToInt32(reader["id"]),
                                Username = Convert.ToString(reader["username"]),
                                EMail = Convert.ToString(reader["email"]),
                                GebTag = Convert.ToDateTime(reader["birthdate"])
                            }
                            );
                    }
                }
            } catch (MySqlException) {
                throw;
            }
            return allUsers.Count >= 1 ? allUsers : null;
        }

        public User GetUser(int userId) {
            try {
                MySqlCommand cmdSelectUser = this.Connection.CreateCommand();
                cmdSelectUser.CommandText = "SELECT * FROM users WHERE id=@id";
                cmdSelectUser.Parameters.AddWithValue("id", userId);

                //"kurze" schreibweise für try finally
                using (MySqlDataReader reader = cmdSelectUser.ExecuteReader()) {
                    reader.Read();
                    return new User {
                        Id = Convert.ToInt32(reader["id"]),
                        Username = Convert.ToString(reader["username"]),
                        EMail = Convert.ToString(reader["email"]),
                        GebTag = Convert.ToDateTime(reader["birthdate"])
                    }
                    ;
                }
            } catch (MySqlException) {
                throw;
            }
        }

        public User GetUser(String user) {
            try {
                MySqlCommand cmdSelectUser = this.Connection.CreateCommand();
                cmdSelectUser.CommandText = "SELECT * FROM users WHERE username=@user";
                cmdSelectUser.Parameters.AddWithValue("user", user);

                using (MySqlDataReader reader = cmdSelectUser.ExecuteReader()) {
                    reader.Read();
                    return new User {
                        Id = Convert.ToInt32(reader["id"]),
                        Username = Convert.ToString(reader["username"]),
                        EMail = Convert.ToString(reader["email"]),
                        GebTag = Convert.ToDateTime(reader["birthdate"])
                    }
                    ;
                }
            } catch (MySqlException) {
                throw;
            }
        }

        public int GetUserId(String user) {
            return GetUser(user).Id;
        }

        public bool Insert(User userToInsert) {
            try {
                MySqlCommand cmdInsert = this.Connection.CreateCommand();
                cmdInsert.CommandText = "INSERT INTO users VALUES(null, @username, sha1(@passwort), @email, @gebtag);";
                cmdInsert.Parameters.AddWithValue("username", userToInsert.Username);
                cmdInsert.Parameters.AddWithValue("passwort", userToInsert.Passwort);
                cmdInsert.Parameters.AddWithValue("email", userToInsert.EMail);
                cmdInsert.Parameters.AddWithValue("gebtag", userToInsert.GebTag);

                return cmdInsert.ExecuteNonQuery() == 1;
            } catch (MySqlException) {
                throw;
            } catch (Exception) {
                throw;
            }
        }

        public List<Artikel> GetWarenkorbFromUser(String user) {
            List<Artikel> allArtikel = new List<Artikel>();
            List<int> allArtikelId = new List<int>();
            try {
                int userId = GetUserId(user);
                MySqlCommand cmdSelectArtikel = this.Connection.CreateCommand();
                cmdSelectArtikel.CommandText = "SELECT * FROM warenkorb WHERE user=@id;";
                cmdSelectArtikel.Parameters.AddWithValue("id", userId);
                using (MySqlDataReader reader = cmdSelectArtikel.ExecuteReader()) {
                    while (reader.Read()) {
                        allArtikelId.Add(Convert.ToInt32(reader["artikel"]));
                    }
                }
                foreach (int i in allArtikelId) {
                    cmdSelectArtikel.CommandText = "SELECT * FROM artikel WHERE id=" + i + ";";
                    using (MySqlDataReader reader = cmdSelectArtikel.ExecuteReader()) {
                        while (reader.Read()) {
                            allArtikel.Add(
                                new Artikel(Convert.ToInt32(reader["id"]),
                                Convert.ToString(reader["name"]),
                                Convert.ToString(reader["beschreibung"]),
                                Convert.ToDecimal(reader["preis"]),
                                Convert.ToString(reader["groesse"]),
                                null));
                        }
                    }
                }
                artikelRepository = ArtikelRepoDB.Instance;
                artikelRepository.Open();
                artikelRepository.SetKategoriesFromMultipleArtikels(allArtikel);
            } catch (MySqlException) {
                throw;
            } finally {
                artikelRepository.Close();
            }
            return allArtikel.Count >= 1 ? allArtikel : null;
        }

        public bool InsertIntoWarenkorb(int userId, int artikelId) {
            try {
                MySqlCommand cmdInsert = this.Connection.CreateCommand();
                cmdInsert.CommandText = "INSERT INTO warenkorb VALUES(@user, @artikel);";
                cmdInsert.Parameters.AddWithValue("user", userId);
                cmdInsert.Parameters.AddWithValue("artikel", artikelId);

                return cmdInsert.ExecuteNonQuery() == 1;
            } catch (MySqlException) {
                throw;
            } catch (Exception) {
                throw;
            }
        }

        public List<Artikel> GetArtikelFromUser(String user) {
            List<Artikel> allArtikel = new List<Artikel>();
            try {
                MySqlCommand cmdSelectArtikel = this.Connection.CreateCommand();

                cmdSelectArtikel.CommandText = "SELECT * FROM artikel WHERE verkaufer=" + GetUserId(user) + ";";
                using (MySqlDataReader reader = cmdSelectArtikel.ExecuteReader()) {
                    while (reader.Read()) {
                        allArtikel.Add(
                            new Artikel(Convert.ToInt32(reader["id"]),
                            Convert.ToString(reader["name"]),
                            Convert.ToString(reader["beschreibung"]),
                            Convert.ToDecimal(reader["preis"]),
                            Convert.ToString(reader["groesse"]),
                            null));
                    }
                }

                artikelRepository = ArtikelRepoDB.Instance;
                artikelRepository.Open();
                artikelRepository.SetKategoriesFromMultipleArtikels(allArtikel);
            } catch (MySqlException) {
                throw;
            } finally {
                artikelRepository.Close();
            }
            return allArtikel.Count >= 1 ? allArtikel : null;
        }
    }
}