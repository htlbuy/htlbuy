﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace HTLBuy.Models.db
{
    public class AuctionRepositoryDB : IAuctionsRepository
    {

        private string ConnectionString = "Server=localhost;Port=3306;Database=htlAuktionen;Uid=root;Pwd=Sarah3618;SSLMode=Preferred";
        private SqlConnection Connection;
        public static int bla;

        private static AuctionRepositoryDB RepoInstance;

        

      
        public static AuctionRepositoryDB Instance
        {
            get
            {
                if (RepoInstance == null)
                {
                    bla = 9;
                    RepoInstance = new AuctionRepositoryDB();
                    bla = 11;
                }
                return RepoInstance;
            }
        }

        public void Open()
        {
            try
            {
                if (this.Connection == null)
                    this.Connection = new SqlConnection(this.ConnectionString);
                if (this.Connection.State != ConnectionState.Open)
                    this.Connection.Open();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void Close()
        {
            try
            {
                if (this.Connection == null)
                    if (this.Connection.State == ConnectionState.Open)
                        this.Connection.Close();
            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}